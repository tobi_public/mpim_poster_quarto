# MPIM Poster Quarto Format

## Installing

There are two main ways of installing this format.

### Create a new Quarto project
```bash
quarto use template https://gitlab.gwdg.de/tobi_public/mpim_poster_quarto/-/archive/main/mpim_poster_quarto-main.zip
```

This will install the extension and create an example qmd file that you can use as a starting place for your article.

### Add to existing Quarto project

If you already have an existing Quarto project an want to add this format to the project, please run:

```bash
quarto add https://gitlab.gwdg.de/tobi_public/mpim_poster_quarto/-/archive/main/mpim_poster_quarto-main.zip
```

This can also be used to update the format to a newer version.

## Using

*TODO*: Describe how to use your format.

## Format Options

*TODO*: If your format has options that can be set via document metadata, describe them.

## Example

Here is the source code for a sample document: [template.qmd](template.qmd).
An rendered version of the document is [available online](https://tobi_public.pages.gwdg.de/mpim_poster_quarto/template.html).
